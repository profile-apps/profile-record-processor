package examples.profile.domain;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

import lombok.Data;

@Data
@CsvRecord(separator = "\\|")
public class Profile {

	@DataField(pos = 1)
	private Long id;

	@DataField(pos = 2)
	private String firstName;
	
	@DataField(pos = 3)
	private String lastName;

	@DataField(pos = 4)
	private String email;

	@DataField(pos = 5)
	private String gender;

	@DataField(pos = 6)
	private String ipAddress;

}


package examples.profile.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.BindyType;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import examples.profile.domain.Profile;

@Component
public class ProfileRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		from("activemq:{{profile.queue.name}}")
		.unmarshal()
			.bindy(BindyType.Csv, Profile.class)
		.marshal()
			.json(JsonLibrary.Jackson, true)
		.setHeader("CamelHttpMethod", constant("POST"))
		.to("{{profile.api.url}}");
		
	}



}
